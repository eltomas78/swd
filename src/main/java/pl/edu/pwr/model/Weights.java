package pl.edu.pwr.model;

import java.util.List;
import java.util.Random;

public class Weights {
	
	private static Random timeRandom;
	private static Random economicRandom;
	private static Random distanceRandom;
	private static final int MAX_BOUND = 40;
	
	public static Integer[][] createDistanceMatrix(List<Point> points){
		int pointsQuantity = points.size();
		Integer[][] distanceMatrix = new Integer[pointsQuantity][pointsQuantity];
		
		for (int i = 0; i < distanceMatrix.length; i++) {
			for (int j = i; j < distanceMatrix.length; j++) {
				if (i == j) {					
					distanceMatrix[i][j] = -1;
				} else {
					Point point1 = points.get(i);
					Point point2 = points.get(j);
					Double x1 = point1.getxAxis();
					Double y1 = point1.getyAxis();
					Double x2 = point2.getxAxis();
					Double y2 = point2.getyAxis();
					Double result = Math.sqrt((x1-x2)*(x1-x2)+(y1-y2)*(y1-y2))*1000;
					distanceMatrix[i][j] = result.intValue();
					distanceMatrix[j][i] = result.intValue();
				}
			}
		}			
		return distanceMatrix;
	}
	
	public static Integer[][] createDistanceRandomMatrix(List<Point> points){
		distanceRandom = new Random(55L);
		int pointsQuantity = points.size();
		Integer[][] distanceRandomMatrix = createRandomMatrix(distanceRandom, pointsQuantity);		
		return distanceRandomMatrix;
	}
	
	public static Integer[][] createTimeRandomMatrix(List<Point> points){
		timeRandom = new Random(13L);
		int pointsQuantity = points.size();
		Integer[][] timeRandomMatrix = createRandomMatrix(timeRandom, pointsQuantity);		
		return timeRandomMatrix;
	}
	
	public static Integer[][] createEconomicRandomMatrix(List<Point> points){
		economicRandom = new Random(79L);
		int pointsQuantity = points.size();
		Integer[][] economicRandomMatrix = createRandomMatrix(economicRandom, pointsQuantity);			
		return economicRandomMatrix;
	}
	
	private static Integer[][] createRandomMatrix(Random random, int matrixSize){		
		Integer[][] randomMatrix = new Integer[matrixSize][matrixSize];
		
		for (int i = 0; i < randomMatrix.length; i++) {
			for (int j = i; j < randomMatrix.length; j++) {
				if (i == j) {					
					randomMatrix[i][j] = -1;
				} else {
					int nextInt = random.nextInt(MAX_BOUND) + 1;
					randomMatrix[i][j] = nextInt;
					randomMatrix[j][i] = nextInt;
				}
			}
		}			
		return randomMatrix;
	}
	
	public static String displayMatrix(Integer[][] matrix){
		String description = "";
		for (int i = 0; i < matrix.length; i++) {
			description += "[";
			for (int j = 0; j < matrix[0].length; j++) {
				description += matrix[i][j] + (j == matrix[0].length - 1? "" : ", ");
			}
			description += "]\n";
		}
		return description;
	}
	
	public enum Matrix {
		
		distance(distanceMatrix),
		time(timeMatrix),
		economic(economicMatrix);
		
		private Integer[][] matrix;
			
		private Matrix() {		
		}

		private Matrix(Integer[][] matrix){
			this.matrix = matrix;
		}
		
		public Integer[][] getMatrix() {
			return matrix;
		}	
		
		public String toString(){
			String description = "";
			for (int i = 0; i < matrix.length; i++) {
				description += "[";
				for (int j = 0; j < matrix[0].length; j++) {
					description += matrix[i][j] + (j == matrix[0].length - 1? "" : ", ");
				}
				description += "]\n";
			}
			return description;
		}
	}
	
	private final static Integer[][] distanceMatrix = new Integer[][]{
		{-1,  3,  7,  4, 12, 14, 17, 21, 13, 16},
		{ 3, -1,  6,  5, 10, 11, 14, 19,  7, 13},
		{ 7,  6, -1,  9, 14, 15, 17, 20, 13, 18},
		{ 4,  5,  9, -1,  6,  7, 11, 16,  6, 11},
		{12, 10, 14,  6, -1,  3,  6, 12, 11, 10},
		{14, 11, 15,  7,  3, -1, 13, 19, 15, 12},
		{17, 14, 17, 11,  6, 13, -1, 12, 20, 21},
		{21, 19, 20, 16, 12, 19, 12, -1, 25, 50},
		{13,  7, 13,  6, 11, 15, 20, 25, -1, 16},
		{16, 13, 18, 11, 10, 12, 21, 50, 16, -1}};
		
	private final static Integer[][] timeMatrix = new Integer[][]{
		{-1, 16, 19, 17, 21, 23, 20, 18, 15, 22},
		{16, -1, 15, 18, 19, 17, 21, 16, 23, 20},
		{19, 15, -1, 20, 22, 18, 16, 19, 17, 21},
		{17, 18, 20, -1, 16, 15, 19,  1,  2, 18},
		{21, 19, 22, 16, -1, 20, 17, 18, 15, 23},
		{23, 17, 18, 15, 20, -1, 16, 19, 22, 21},
		{20, 21, 16, 19, 17, 16, -1, 18, 15, 22},
		{18, 16, 19,  1, 18, 19, 18, -1, 23, 49},
		{15, 23, 17,  2, 15, 22, 15, 23, -1,  3},
		{22, 20, 21, 18, 23, 21, 22, 49,  3, -1}};
			
	private final static Integer[][] economicMatrix = new Integer[][]{
		{-1, 16,  1, 18, 19, 17, 15, 22, 21,  2},
		{16, -1, 23, 19, 17, 20, 21, 16, 15, 18},
		{ 1, 23, -1, 16, 15, 19, 17,  3, 18, 20},
		{18, 19, 16, -1, 16, 17, 20, 15, 22, 21},
		{19, 17, 15, 16, -1, 20, 22, 18, 21, 23},
		{17, 20, 19, 17, 20, -1, 15, 22, 21, 18},
		{15, 21, 17, 20, 22, 15, -1, 16, 18, 19},
		{22, 16,  3, 15, 18, 22, 16, -1, 17, 48},
		{21, 15, 18, 22, 21, 21, 18, 17, -1, 16},
		{ 2, 18, 20, 21, 23, 18, 19, 48, 16, -1}};
		
}
