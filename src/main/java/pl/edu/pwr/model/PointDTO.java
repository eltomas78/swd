package pl.edu.pwr.model;

import java.io.Serializable;

public class PointDTO implements Serializable{

	private static final long serialVersionUID = 4174107381260076903L;
	
	private Double xAxis, yAxis;

	public Double getxAxis() {
		return xAxis;
	}

	public void setxAxis(Double xAxis) {
		this.xAxis = xAxis;
	}

	public Double getyAxis() {
		return yAxis;
	}

	public void setyAxis(Double yAxis) {
		this.yAxis = yAxis;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((xAxis == null) ? 0 : xAxis.hashCode());
		result = prime * result + ((yAxis == null) ? 0 : yAxis.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof PointDTO))
			return false;
		PointDTO other = (PointDTO) obj;
		if (xAxis == null) {
			if (other.xAxis != null)
				return false;
		} else if (!xAxis.equals(other.xAxis))
			return false;
		if (yAxis == null) {
			if (other.yAxis != null)
				return false;
		} else if (!yAxis.equals(other.yAxis))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "PointDTO [xAxis=" + xAxis + ", yAxis=" + yAxis + "]";
	}	
}
