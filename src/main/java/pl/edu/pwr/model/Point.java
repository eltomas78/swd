package pl.edu.pwr.model;

import java.io.Serializable;

public class Point implements Serializable{

	private static final long serialVersionUID = -5207583565725748804L;
	
	private Integer id;
	private Double xAxis, yAxis;
	
	public Point(){		
	}
	
	public Point(Integer id, Double xAxis, Double yAxis){
		this.id = id;
		this.xAxis = xAxis;
		this.yAxis = yAxis;
	}
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Double getxAxis() {
		return xAxis;
	}
	public void setxAxis(Double xAxis) {
		this.xAxis = xAxis;
	}
	public Double getyAxis() {
		return yAxis;
	}
	public void setyAxis(Double yAxis) {
		this.yAxis = yAxis;
	}	
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		result = prime * result + ((xAxis == null) ? 0 : xAxis.hashCode());
		result = prime * result + ((yAxis == null) ? 0 : yAxis.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (!(obj instanceof Point))
			return false;
		Point other = (Point) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		if (xAxis == null) {
			if (other.xAxis != null)
				return false;
		} else if (!xAxis.equals(other.xAxis))
			return false;
		if (yAxis == null) {
			if (other.yAxis != null)
				return false;
		} else if (!yAxis.equals(other.yAxis))
			return false;
		return true;
	}

	@Override
	public String toString() {
		return "Point [id=" + id + ", xAxis=" + xAxis + ", yAxis=" + yAxis + "]";
	}	
}
