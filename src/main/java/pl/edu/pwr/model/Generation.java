package pl.edu.pwr.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Component;

import pl.edu.pwr.comparator.MultiCriteriaGeneComparator;
import pl.edu.pwr.function.DistanceObjectiveFunction;
import pl.edu.pwr.function.EconomicObjectiveFunction;
import pl.edu.pwr.function.MultiCriteriaObjectiveFunction;
import pl.edu.pwr.function.NonPreferentialObjectiveFunction;
import pl.edu.pwr.function.ObjectiveFunction;
import pl.edu.pwr.function.TimeObjectiveFunction;

public class Generation {

	private int population;
	private double mutationProbability;
	private double crossoverProbability;
	private List<Gene> genes;
	
	private DistanceObjectiveFunction distanceObjectiveFunction = new DistanceObjectiveFunction();
	private TimeObjectiveFunction timeObjectiveFunction = new TimeObjectiveFunction();
	private EconomicObjectiveFunction economicObjectiveFunction = new EconomicObjectiveFunction();
			
	public Generation(int population, double mutationProbability, double crossoverProbability) {
		this.population = population;
		this.mutationProbability = mutationProbability;
		this.crossoverProbability = crossoverProbability;
		this.genes = new ArrayList<Gene>();
	}

	public List<Gene> getGenes() {
		return this.genes;
	}

	public void setGenes(List<Gene> genes) {
		this.genes = genes;
	}
	
	public void createRandomisedGeneration(int indexOfStartPoint, List<Point> points){
		for (int i = 0; i < population; i++) {
			Gene gene = new Gene();
			gene.randomiseGene(indexOfStartPoint, points);
			this.genes.add(gene);
		}		
	}	
	
	public Integer calculateGenerationAndReturnExtremum(int indexOfEndPoint, ObjectiveFunction function, boolean minimum){
		int extremum = minimum? Integer.MAX_VALUE : Integer.MIN_VALUE;
		
		for (int i = 0; i < this.population; i++) {
			Gene gene = this.genes.get(i);
			Integer genesValue = gene.calculateFunction(indexOfEndPoint, function);
			if (minimum && genesValue < extremum) {
				extremum = genesValue;
			}
			else if (!minimum && genesValue > extremum) {
				extremum = genesValue;
			}
			System.out.println("gene = " + gene + ", genesValue = " + genesValue + ", extremum = " + extremum);
		}	
		return extremum;
	}
	
	private int calculateGenerationAndReturnExtremum2(List<Gene> passedGenes, int indexOfEndPoint,
			ObjectiveFunction function, boolean minimum) {
		
		int extremum = minimum? Integer.MAX_VALUE : Integer.MIN_VALUE;
		
		for (int i = 0; i < passedGenes.size(); i++) {
			Gene gene = passedGenes.get(i);
			Integer genesValue = gene.calculateFunction(indexOfEndPoint, function);
			if (minimum && genesValue < extremum) {
				extremum = genesValue;
			}
			else if (!minimum && genesValue > extremum) {
				extremum = genesValue;
			}
			System.out.println("gene2 = " + gene + ", genesValue = " + genesValue + ", extremum = " + extremum);
		}	
		return extremum;
	}
	
	public Gene calculateGenerationAndReturnMultiCriteriaOptimalValue(int indexOfEndPoint, MultiCriteriaObjectiveFunction function, int[] bestValues, String criteriaSequence){
		int minimum = Integer.MAX_VALUE;
		Gene bestGene = null;
		if (function instanceof NonPreferentialObjectiveFunction) {
			for (int i = 0; i < this.population; i++) {
				Gene gene = this.genes.get(i);
				Integer genesValue = gene.calculateMultiCriteriaNonPrefFunction(indexOfEndPoint, function, bestValues, criteriaSequence);
				if (genesValue < minimum) {
					minimum = genesValue;
					bestGene = gene;
				}			
				System.out.println("NON_PREF: gene = " + gene + ", genesValue = " + genesValue + ", minimum = " + minimum);
			}	
		} else {
			//najpierw zwracam z calego pokolenia liste genow spelniajace pierwsze kryterium plus delta 
			//pozniej z tej listy wyznaczam najlepsza wartosc: bestValues[1];
			//pozniej znowu iteruje po zwroconej wczesniej liscie i zwracam nowa liste spelniajace kryterium plus delta
			//ponownie z tej listy wyznaczam najlepsze kryterium: bestValues[2];
			//z tej pozostalosci wyznaczam liste genow spelniajace ostatnie kryterium plus delta
			
			List<Gene> passedGenes = new ArrayList<Gene>();
			for (int i = 0; i < 3; i++) {// podwojna petla??? //this.population
				if (i == 0) {
					for (int j = 0; j < this.population; j++) {
						Gene gene = this.genes.get(j);
						Integer genesValue = gene.calculateMultiCriteriaPrefFunction(indexOfEndPoint, function, bestValues, i, criteriaSequence);//TODO tutaj trzba rozstrzygnac jaki GEN - ROAD zwrocic!!!
						System.out.println("["+i+"]["+j+"]: genesValue: " + genesValue);
						if (genesValue > 0) {
							passedGenes.add(gene);
						}
					}	
				} else {
					//obliczyc bestValues dla [1] i [2]					
					if (criteriaSequence.startsWith("T", i)) {						
						bestValues[i] = calculateGenerationAndReturnExtremum2(passedGenes, indexOfEndPoint, distanceObjectiveFunction, true);
					} else if (criteriaSequence.startsWith("C", i)){						
						bestValues[i] = calculateGenerationAndReturnExtremum2(passedGenes, indexOfEndPoint, timeObjectiveFunction, true);
					} else if (criteriaSequence.startsWith("P", i)){		
						bestValues[i] = calculateGenerationAndReturnExtremum2(passedGenes, indexOfEndPoint, economicObjectiveFunction, true);
					}							
					
					for (int j = 0; j < passedGenes.size(); j++) {//musze usuwac od konca!!! //ryzykowne - IndexOutOfBound
						Gene gene = passedGenes.get(j);						
						Integer genesValue = gene.calculateMultiCriteriaPrefFunction(indexOfEndPoint, function, bestValues, i, criteriaSequence);//TODO tutaj trzba rozstrzygnac jaki GEN - ROAD zwrocic!!!
						System.out.println("["+i+"]["+j+"]: genesValue: " + genesValue);
						if (genesValue < 0) {
							passedGenes.remove(j);
						} 
					}
				}
					
				//System.out.println("PREF: gene = " + gene + ", genesValue = " + genesValue + ", minimum = " + minimum);		
				System.out.println("i = " + i + ", passedGenes.size: " + passedGenes.size() + ", passedGenes: " + passedGenes);
				System.out.println("bestValues[0] = " + bestValues[0]);
				System.out.println("bestValues[1] = " + bestValues[1]);
				System.out.println("bestValues[2] = " + bestValues[2]);
			}
			if (!passedGenes.isEmpty()) {
				bestGene = passedGenes.get(0);
			}
		}			
		return bestGene;
	}		

	public Generation makeGeneSelection(int groupTournamentSize, int indexOfEndPoint, MultiCriteriaObjectiveFunction function, int[] bestValues, String criteriaSequence){	
		Generation newGeneration = new Generation(this.population, this.mutationProbability, this.crossoverProbability);	
		int counter = 0;
		while (newGeneration.getGenes().size() < this.population) {
			List<Gene> tournamentGroup = new ArrayList<Gene>();
			for (int i = 0; i < groupTournamentSize && i < this.population; i++) {
				counter %= this.population;
				tournamentGroup.add(this.genes.get(counter++));
			}			
			tournamentGroup.sort(new MultiCriteriaGeneComparator(indexOfEndPoint, function, bestValues, criteriaSequence));			
			Gene winner = tournamentGroup.get(0);
			List<Point> points = winner.getPoints();
			points = winner.copyList(points);
			Gene gene = new Gene();
			gene.setPoints(points);
			newGeneration.getGenes().add(gene);				
		}		
		return newGeneration;
	}
	
	public void mutate(){
		Random random = new Random();			
		for (int i = 0; i < this.population; i++) {
			if(random.nextDouble() < this.mutationProbability){
				Gene gene = this.genes.get(i);
				List<Point> points = gene.getPoints();				
				int pointsQuantity = points.size();				
				List<Integer> swapPointsIndexes = getSwapPointsIndexes(pointsQuantity - 1);//first Point has always starting point value!
				int firstIndexToSwap = swapPointsIndexes.get(0);
				int secondIndexToSwap = swapPointsIndexes.get(1);				
				points = swapPoints(points, firstIndexToSwap, secondIndexToSwap);					
				gene.setPoints(points);				
			}			
		}		
		//System.out.println("genes (mutate):" + this.toString());
	}

	private List<Integer> getSwapPointsIndexes(int quantity){		
		List<Integer> listOfIndexes = new ArrayList<Integer>(); 
		List<Integer> indexesOfSwapPoints = new ArrayList<Integer>();
		int swapQuantity = 2;		
		for (int i = 0; i < quantity; i++) {
			listOfIndexes.add(i + 1);//first Point has always starting point value!
		}
		
		while(swapQuantity > 0){
			Random random = new Random();
			int swapPointIndex = random.nextInt(quantity);
			indexesOfSwapPoints.add(listOfIndexes.get(swapPointIndex));
			listOfIndexes.remove(swapPointIndex);
			quantity--;
			swapQuantity--;
		}		
		return indexesOfSwapPoints;
	}
	
	private List<Point> swapPoints(List<Point> points, int firstIndexToSwap, int secondIndexToSwap) {
		Point tempPoint = points.get(firstIndexToSwap);
		points.set(firstIndexToSwap, points.get(secondIndexToSwap));
		points.set(secondIndexToSwap, tempPoint);		
		return points;		
	}	
	
	public void crossover(){
		Random random = new Random();			
		for (int i = 0; i < this.population;) {
			if(i + 1 < this.population && random.nextDouble() < this.crossoverProbability){
				Gene gene1 = this.genes.get(i);
				Gene gene2 = this.genes.get(i + 1);
				//System.out.println("gene1: " + gene1);
				//System.out.println("gene2: " + gene2);
				List<Point> pointsOfGene1 = gene1.getPoints();
				List<Point> pointsOfGene2 = gene2.getPoints();
				int quantityOfPoints = gene1.getPoints().size();
				List<Point> pointsOfCrossOverGene1AndGene2 = new ArrayList<Point>();
				List<Point> pointsOfCrossOverGene2AndGene1 = new ArrayList<Point>();				
				initialisePointsList(pointsOfCrossOverGene1AndGene2, pointsOfGene1);
				initialisePointsList(pointsOfCrossOverGene2AndGene1, pointsOfGene2);				
				int subsetOfPointsSize = quantityOfPoints / 2;
				int startIndexOfSubsetOfPoints = subsetOfPointsSize - (subsetOfPointsSize / 2);				
				List<Point> subListGene1 = pointsOfGene1.subList(startIndexOfSubsetOfPoints, startIndexOfSubsetOfPoints + subsetOfPointsSize);
				List<Point> subListGene2 = pointsOfGene2.subList(startIndexOfSubsetOfPoints, startIndexOfSubsetOfPoints + subsetOfPointsSize);
				//System.out.println("subListGene1: " + subListGene1);
				//System.out.println("subListGene2: " + subListGene2);
				setInitialCrossOverSubsets(startIndexOfSubsetOfPoints, subListGene1, pointsOfCrossOverGene1AndGene2);
				setInitialCrossOverSubsets(startIndexOfSubsetOfPoints, subListGene2, pointsOfCrossOverGene2AndGene1);				
				//System.out.println("pointsOfCrossOverGene1AndGene2: " + pointsOfCrossOverGene1AndGene2);
				//System.out.println("pointsOfCrossOverGene2AndGene1: " + pointsOfCrossOverGene2AndGene1);				
				finaliseCrossOver(startIndexOfSubsetOfPoints + subsetOfPointsSize, quantityOfPoints, pointsOfGene2, pointsOfCrossOverGene1AndGene2);
				finaliseCrossOver(startIndexOfSubsetOfPoints + subsetOfPointsSize, quantityOfPoints, pointsOfGene1, pointsOfCrossOverGene2AndGene1);
				gene1.setPoints(pointsOfCrossOverGene1AndGene2);
				gene2.setPoints(pointsOfCrossOverGene2AndGene1);				
			}	
			i += 2;
		}		
	}	
	
	private void initialisePointsList(List<Point> pointsOfCrossOver, List<Point> pointsOfGene) {
		int listSize = pointsOfGene.size();
		Point firstGene = pointsOfGene.get(0);
		pointsOfCrossOver.add(firstGene);
		for (int i = 1; i < listSize; i++) {
			pointsOfCrossOver.add(null);
		}		
	}

	private void setInitialCrossOverSubsets(int startIndexOfSubsetOfPoints, List<Point> subListGene,
			List<Point> pointsOfCrossOver) {
		int sublistGeneSize = subListGene.size();
		for (int i = 0; i < sublistGeneSize; i++) {
			Point point = subListGene.get(i);
			pointsOfCrossOver.set(startIndexOfSubsetOfPoints + i, point);
		}		
	}
	
	private void finaliseCrossOver(int endIndexOfSubsetOfPoints, int quantityOfPoints, List<Point> pointsOfGene, List<Point> pointsOfCrossOver) {		
		int childIndex = endIndexOfSubsetOfPoints % quantityOfPoints;		
		for (int j = endIndexOfSubsetOfPoints; j < endIndexOfSubsetOfPoints + quantityOfPoints; j++) {
			int parentIndex = j % quantityOfPoints;			
			Point point = pointsOfGene.get(parentIndex);			
			if (!pointsOfCrossOver.contains(point)) {				
				pointsOfCrossOver.set(childIndex, point);				
				childIndex = (childIndex + 1) % quantityOfPoints;
				childIndex = childIndex == 0? 1 : childIndex;//make sure first Gene is always set with starting point value (cannot be overwritten)!
			}
		}
		//System.out.println("fulfilled pointsOfCrossOver: " + pointsOfCrossOver);		
	}
	
	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("Generation. Genes:\n");
		for (int i = 0; i < this.genes.size(); i++) {
			Gene gene = this.genes.get(i);
			sb.append(gene + "\n");
		}		
		return sb.toString();		
	}	
}
