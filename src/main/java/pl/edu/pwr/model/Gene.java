package pl.edu.pwr.model;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;

import pl.edu.pwr.function.MultiCriteriaObjectiveFunction;
import pl.edu.pwr.function.ObjectiveFunction;

public class Gene {
	
	private List<Point> points;
	private List<Point> requestedRoad;
	
	public Gene() {
		points = new ArrayList<Point>();
		requestedRoad = new ArrayList<Point>();
	}
	
	public List<Point> getPoints() {
		return points;
	}

	public void setPoints(List<Point> points) {
		this.points = points;
	}	
	
	public List<Point> getRequestedRoad() {
		return requestedRoad;
	}

	public void setRequestedRoad(List<Point> requestedRoad) {
		this.requestedRoad = requestedRoad;
	}

	
	public void randomiseGene(int indexOfStartPoint, List<Point> points){
		this.requestedRoad = points;
		List<Point> copiedPoints = copyList(points);		
		Point startPoint = copiedPoints.get(indexOfStartPoint);
		this.points.add(startPoint);//every Gene will have the same starting point
		copiedPoints.remove(indexOfStartPoint);			
		while (copiedPoints.size() > 0) {
			int pointsQuantity = copiedPoints.size();			
			Random random = new Random();
			int indexOfSelectedPoint = random.nextInt(pointsQuantity);
			Point selectedPoint = copiedPoints.get(indexOfSelectedPoint);
			this.points.add(selectedPoint);	
			copiedPoints.remove(indexOfSelectedPoint);					
		}		
	}
	protected List<Point> copyList(List<Point> points){
		int pointsQuantity = points.size();
		List<Point> copiedList = new ArrayList<Point>(pointsQuantity);
		for (int i = 0; i < pointsQuantity; i++) {
			Point point = points.get(i);
			Point copiedPoint = new Point();
			copiedPoint.setId(point.getId());
			copiedPoint.setxAxis(point.getxAxis());
			copiedPoint.setyAxis(point.getyAxis());
			copiedList.add(copiedPoint);			
		}		
		return copiedList;
	}
	
	public Integer calculateFunction(int indexOfEndPoint, ObjectiveFunction function) {
		Integer value = function.calculateGenesValue(indexOfEndPoint, this);		
		return value;
	}	
	
	public Integer calculateMultiCriteriaNonPrefFunction(int indexOfEndPoint, MultiCriteriaObjectiveFunction function, int[] bestValues, String criteriaSequence) {
		int DUMMY_BEST_VALUES_INDEX = 0;
		Integer value = function.calculateObjectiveFunction(indexOfEndPoint, this, bestValues, DUMMY_BEST_VALUES_INDEX, criteriaSequence);		
		return value;
	}	
	
	public Integer calculateMultiCriteriaPrefFunction(int indexOfEndPoint, MultiCriteriaObjectiveFunction function, int[] bestValues, int bestValuesIndex, String criteriaSequence) {
		Integer value = function.calculateObjectiveFunction(indexOfEndPoint, this, bestValues, bestValuesIndex, criteriaSequence);		
		return value;
	}	

	@Override
	public String toString() {
		StringBuilder sb = new StringBuilder("[");
		for (int i = 0; i < this.points.size(); i++) {
			Point point = this.points.get(i);
			sb.append("id = " + point.getId() + (i == this.points.size() - 1? "" : ", "));
		}
		sb.append("]");
		return sb.toString();
	}	
}
