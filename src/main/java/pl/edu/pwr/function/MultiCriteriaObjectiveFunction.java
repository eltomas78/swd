package pl.edu.pwr.function;

import pl.edu.pwr.model.Gene;

public interface MultiCriteriaObjectiveFunction {
	
	Integer calculateObjectiveFunction(int indexOfEndPoint, Gene gene, int[] bestValues, int bestValueIndex, String criteriaSequence);
}
