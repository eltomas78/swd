package pl.edu.pwr.function;

import java.util.List;

import org.springframework.stereotype.Component;

import pl.edu.pwr.model.Gene;
import pl.edu.pwr.model.Point;
import pl.edu.pwr.model.Weights;

@Component("distanceObjectiveFunction")
public class DistanceObjectiveFunction implements ObjectiveFunction {
	
	private Integer[][] distanceMatrix = Weights.Matrix.distance.getMatrix();

	@Override
	public Integer calculateGenesValue(int indexOfEndPoint, Gene gene) {		
		Integer value = 0;
		List<Point> points = gene.getPoints();
		int pointsQuantity = points.size();
		//Integer[][] distanceMatrix = Weights.createDistanceMatrix(gene.getRequestedRoad());
		//Integer[][] distanceMatrix = Weights.createDistanceRandomMatrix(gene.getRequestedRoad());
		System.out.println("DISTANCE MATRIX:");
		System.out.println(Weights.displayMatrix(distanceMatrix));
		for (int i = 0; i < pointsQuantity; i++) {
			if (i + 1 < pointsQuantity) {
				Point previousPoint = points.get(i);
				Point nextPoint = points.get(i + 1);
				Integer previousPointId = previousPoint.getId();
				Integer nextPointId = nextPoint.getId();
				value  += distanceMatrix[previousPointId][nextPointId];
				if (nextPointId == indexOfEndPoint) {
					break;
				}
			}			
		}		
		return value;
	}
}
