package pl.edu.pwr.function;

import java.util.List;

import org.springframework.stereotype.Component;

import pl.edu.pwr.model.Gene;
import pl.edu.pwr.model.Point;
import pl.edu.pwr.model.Weights;

@Component("preferentialObjectiveFunction")
public class PreferentialObjectiveFunction implements MultiCriteriaObjectiveFunction {

	private Integer[][] distanceMatrix = Weights.Matrix.distance.getMatrix();
	private Integer[][] timeMatrix = Weights.Matrix.time.getMatrix();
	private Integer[][] economicMatrix = Weights.Matrix.economic.getMatrix();

	@Override
	public Integer calculateObjectiveFunction(int indexOfEndPoint, Gene gene, int[] bestValues, int bestValuesIndex, String criteriaSequence) {
//		Integer distanceValue = 0;
//		Integer timeValue = 0;
//		Integer economicValue = 0;
		Integer criteriaValue = 0;
		Integer returnValue = Integer.MIN_VALUE;
		Double epsilon = 0.4;
		List<Point> points = gene.getPoints();
		int pointsQuantity = points.size();	
		//Integer[][] distanceMatrix = Weights.createDistanceMatrix(gene.getRequestedRoad());
		Integer[][] matrix = null;
//		Integer[][] distanceMatrix = Weights.createDistanceRandomMatrix(gene.getRequestedRoad());
//		Integer[][] timeMatrix = Weights.createTimeRandomMatrix(points);
//		Integer[][] economicMatrix = Weights.createEconomicRandomMatrix(points);		
		
		if (criteriaSequence.startsWith("T", bestValuesIndex)) {						
			//matrix = Weights.createDistanceRandomMatrix(points);
			matrix = distanceMatrix;
		} else if (criteriaSequence.startsWith("C", bestValuesIndex)){						
			//matrix = Weights.createTimeRandomMatrix(points);
			matrix = timeMatrix;
		} if (criteriaSequence.startsWith("P", bestValuesIndex)){		
			//matrix = Weights.createEconomicRandomMatrix(points);
			matrix = economicMatrix;
		}			
		
		for (int i = 0; i < pointsQuantity; i++) {
			if (i + 1 < pointsQuantity) {
				Point previousPoint = points.get(i);
				Point nextPoint = points.get(i + 1);
				Integer previousPointId = previousPoint.getId();
				Integer nextPointId = nextPoint.getId();
				criteriaValue  += matrix[previousPointId][nextPointId];
				//timeValue  += timeMatrix[previousPointId][nextPointId];
				//economicValue  += economicMatrix[previousPointId][nextPointId];
				if (nextPointId == indexOfEndPoint) {
					break;
				}
			}			
		}	
//		System.out.println("bestValues[0] = " + bestValues[0] + "; (1 + epsilon) * bestValues[0] = " + (1 + epsilon) * bestValues[0]);
//		System.out.println("bestValues[1] = " + bestValues[1] + "; (1 + epsilon) * bestValues[1] = " + (1 + epsilon) * bestValues[1]);
//		System.out.println("bestValues[2] = " + bestValues[2] + "; (1 + epsilon) * bestValues[2] = " + (1 + epsilon) * bestValues[2]);
		if (criteriaValue <= (1 + epsilon) * bestValues[bestValuesIndex]) {
			returnValue = criteriaValue;
		}		
		return returnValue;
	}
}
