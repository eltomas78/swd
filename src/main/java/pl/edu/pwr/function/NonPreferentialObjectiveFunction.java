package pl.edu.pwr.function;

import java.util.List;

import org.springframework.stereotype.Component;

import pl.edu.pwr.model.Gene;
import pl.edu.pwr.model.Point;
import pl.edu.pwr.model.Weights;

@Component("nonPreferentialObjectiveFunction")
public class NonPreferentialObjectiveFunction implements MultiCriteriaObjectiveFunction {
	
	private Integer[][] distanceMatrix = Weights.Matrix.distance.getMatrix();
	private Integer[][] timeMatrix = Weights.Matrix.time.getMatrix();
	private Integer[][] economicMatrix = Weights.Matrix.economic.getMatrix();

	@Override
	public Integer calculateObjectiveFunction(int indexOfEndPoint, Gene gene, int[] bestValues, int bestValuesIndex, String criteriaSequence) {
		Integer distanceValue = 0;
		Integer timeValue = 0;
		Integer economicValue = 0;
		Integer returnValue = 0;
		List<Point> points = gene.getPoints();
		int pointsQuantity = points.size();
		//Integer[][] distanceMatrix = Weights.createDistanceMatrix(gene.getRequestedRoad());
		//Integer[][] distanceMatrix = Weights.createDistanceRandomMatrix(gene.getRequestedRoad());
		//Integer[][] timeMatrix = Weights.createTimeRandomMatrix(points);
		//Integer[][] economicMatrix = Weights.createEconomicRandomMatrix(points);
		for (int i = 0; i < pointsQuantity; i++) {
			if (i + 1 < pointsQuantity) {
				Point previousPoint = points.get(i);
				Point nextPoint = points.get(i + 1);
				Integer previousPointId = previousPoint.getId();
				Integer nextPointId = nextPoint.getId();
				distanceValue  += distanceMatrix[previousPointId][nextPointId];
				timeValue  += timeMatrix[previousPointId][nextPointId];
				economicValue  += economicMatrix[previousPointId][nextPointId];
				if (nextPointId == indexOfEndPoint) {
					break;
				}
			}			
		}		
		System.out.println("distanceValue: " + distanceValue + ", bestValues[0] = " + bestValues[0] + ", timeValue = " + timeValue + ", bestValues[1] = " + bestValues[1] + ", economicValue = " + economicValue + ", bestValues[2] = "  + bestValues[2]);
		returnValue = Math.abs(distanceValue - bestValues[0]) + Math.abs(timeValue - bestValues[1]) + Math.abs(economicValue - bestValues[2]);
		return returnValue;
	}
}
