package pl.edu.pwr.function;

import java.util.List;

import org.springframework.stereotype.Component;

import pl.edu.pwr.model.Gene;
import pl.edu.pwr.model.Point;
import pl.edu.pwr.model.Weights;

@Component("economicObjectiveFunction")
public class EconomicObjectiveFunction implements ObjectiveFunction {

	private Integer[][] economicMatrix = Weights.Matrix.economic.getMatrix();

	@Override
	public Integer calculateGenesValue(int indexOfEndPoint, Gene gene) {
		Integer value = 0;
		List<Point> points = gene.getPoints();
		int pointsQuantity = points.size();
		//Integer[][] economicMatrix = Weights.createEconomicRandomMatrix(points);
		System.out.println("ECONOMIC MATRIX:");
		System.out.println(Weights.displayMatrix(economicMatrix));
		for (int i = 0; i < pointsQuantity; i++) {
			if (i + 1 < pointsQuantity) {
				Point previousPoint = points.get(i);
				Point nextPoint = points.get(i + 1);
				Integer previousPointId = previousPoint.getId();
				Integer nextPointId = nextPoint.getId();
				value  += economicMatrix[previousPointId][nextPointId];
				if (nextPointId == indexOfEndPoint) {
					break;
				}
			}			
		}		
		return value;
	}
}
