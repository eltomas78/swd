package pl.edu.pwr.function;

import java.util.List;

import org.springframework.stereotype.Component;

import pl.edu.pwr.model.Gene;
import pl.edu.pwr.model.Point;
import pl.edu.pwr.model.Weights;

@Component("timeObjectiveFunction")
public class TimeObjectiveFunction implements ObjectiveFunction {

	private Integer[][] timeMatrix = Weights.Matrix.time.getMatrix();

	@Override
	public Integer calculateGenesValue(int indexOfEndPoint, Gene gene) {
		Integer value = 0;
		List<Point> points = gene.getPoints();
		int pointsQuantity = points.size();
		//Integer[][] timeMatrix = Weights.createTimeRandomMatrix(points);
		System.out.println("TIME MATRIX:");
		System.out.println(Weights.displayMatrix(timeMatrix));
		for (int i = 0; i < pointsQuantity; i++) {
			if (i + 1 < pointsQuantity) {
				Point previousPoint = points.get(i);
				Point nextPoint = points.get(i + 1);
				Integer previousPointId = previousPoint.getId();
				Integer nextPointId = nextPoint.getId();
				value  += timeMatrix[previousPointId][nextPointId];
				if (nextPointId == indexOfEndPoint) {
					break;
				}
			}			
		}		
		return value;
	}
}
