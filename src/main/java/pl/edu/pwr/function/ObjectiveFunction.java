package pl.edu.pwr.function;

import pl.edu.pwr.model.Gene;

public interface ObjectiveFunction {	
	
	Integer calculateGenesValue(int indexOfEndPoint, Gene gene);
}
