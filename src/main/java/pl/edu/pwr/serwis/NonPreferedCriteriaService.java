package pl.edu.pwr.serwis;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import pl.edu.pwr.function.MultiCriteriaObjectiveFunction;
import pl.edu.pwr.function.NonPreferentialObjectiveFunction;
import pl.edu.pwr.function.ObjectiveFunction;
import pl.edu.pwr.model.Gene;
import pl.edu.pwr.model.Generation;
import pl.edu.pwr.model.Point;

@Service("nonPreferedCriteriaService")
public class NonPreferedCriteriaService implements CriteriaService {
	
	@Autowired
	@Qualifier("distanceObjectiveFunction")
	private ObjectiveFunction distanceObjectiveFunction;
	
	@Autowired
	@Qualifier("economicObjectiveFunction")
	private ObjectiveFunction economicObjectiveFunction;
	
	@Autowired
	@Qualifier("timeObjectiveFunction")
	private ObjectiveFunction timeObjectiveFunction;
	
	@Autowired
	@Qualifier("nonPreferentialObjectiveFunction")
	private MultiCriteriaObjectiveFunction nonPreferentialObjectiveFunction;
	
	private Generation generation;
	private Generation generation2;
	private int[] bestValues = new int[3];//distance = [0], time = [1], economic = [2]
	
	@Override
	public List<Point> getOptimalRoad(Integer startPointId, Integer endPointId, String criteriaSequence,
			List<Point> points) {	
		
		generation = new Generation(POPULATION, MUTATION_PROBABILITY, HYBRIDIZATION_PROBABILLITY);
		generation.createRandomisedGeneration(startPointId, points);
		
		
		System.out.println("DISTANCE:");
		bestValues[0] = generation.calculateGenerationAndReturnExtremum(endPointId, distanceObjectiveFunction, MINIMUM);
		System.out.println("TIME:");
		bestValues[1] = generation.calculateGenerationAndReturnExtremum(endPointId, timeObjectiveFunction, MINIMUM);
		System.out.println("ECONOMIC:");
		bestValues[2] = generation.calculateGenerationAndReturnExtremum(endPointId, economicObjectiveFunction, MINIMUM);
		
		//generation2 = new Generation(POPULATION, MUTATION_PROBABILITY, HYBRIDIZATION_PROBABILLITY);
		//generation2.createRandomisedGeneration(startPointId, points);
		
		System.out.println("bestValues[0] = " + bestValues[0]);
		System.out.println("bestValues[1] = " + bestValues[1]);
		System.out.println("bestValues[2] = " + bestValues[2]);
		
		System.out.println("NON_PREFERENTIAL:");		
		Gene bestGene = generation.calculateGenerationAndReturnMultiCriteriaOptimalValue(endPointId, nonPreferentialObjectiveFunction, bestValues, criteriaSequence);
		System.out.println("bestGene = " + bestGene);
		
		List<Point> returnedPoints = bestGene.getPoints();
		int endIndex = 0;
		for (int i = 0; i < returnedPoints.size(); i++) {
			if (returnedPoints.get(i).getId() == endPointId) {
				endIndex = i;
				break;
			}
		}		
		
		return returnedPoints.subList(0, endIndex + 1);
	}

}
