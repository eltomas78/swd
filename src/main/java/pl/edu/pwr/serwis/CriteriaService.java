package pl.edu.pwr.serwis;

import java.util.List;

import pl.edu.pwr.model.Point;

public interface CriteriaService {
	final int POPULATION = 500;
	final double MUTATION_PROBABILITY = 0.01;
	final double HYBRIDIZATION_PROBABILLITY = 0.25;
	final boolean MINIMUM = true;
	
	List<Point> getOptimalRoad(Integer startPointId, Integer endPointId, String criteriaSequence, List<Point> points);

}
