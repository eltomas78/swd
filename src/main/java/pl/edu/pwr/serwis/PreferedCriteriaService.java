package pl.edu.pwr.serwis;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Service;

import pl.edu.pwr.function.MultiCriteriaObjectiveFunction;
import pl.edu.pwr.function.ObjectiveFunction;
import pl.edu.pwr.model.Gene;
import pl.edu.pwr.model.Generation;
import pl.edu.pwr.model.Point;

@Service("preferedCriteriaService")
public class PreferedCriteriaService implements CriteriaService {
	
	@Autowired
	@Qualifier("distanceObjectiveFunction")
	private ObjectiveFunction distanceObjectiveFunction;
	
	@Autowired
	@Qualifier("economicObjectiveFunction")
	private ObjectiveFunction economicObjectiveFunction;
	
	@Autowired
	@Qualifier("timeObjectiveFunction")
	private ObjectiveFunction timeObjectiveFunction;
	
	@Autowired
	@Qualifier("preferentialObjectiveFunction")
	private MultiCriteriaObjectiveFunction preferentialObjectiveFunction;
	
	private Generation generation;	
	private int[] bestValues;
	

	@Override
	public List<Point> getOptimalRoad(Integer startPointId, Integer endPointId, String criteriaSequence,
			List<Point> points) {
		
		generation = new Generation(POPULATION, MUTATION_PROBABILITY, HYBRIDIZATION_PROBABILLITY);
		generation.createRandomisedGeneration(startPointId, points);
		bestValues = new int[3];//distance = [0], time = [1], economic = [2]
		
		if (criteriaSequence.startsWith("T")) {
			System.out.println("DISTANCE:");
			bestValues[0] = generation.calculateGenerationAndReturnExtremum(endPointId, distanceObjectiveFunction, MINIMUM);
		} else if (criteriaSequence.startsWith("C")){
			System.out.println("TIME:");
			bestValues[0] = generation.calculateGenerationAndReturnExtremum(endPointId, timeObjectiveFunction, MINIMUM);
		} else {
			System.out.println("ECONOMIC:");
			bestValues[0] = generation.calculateGenerationAndReturnExtremum(endPointId, economicObjectiveFunction, MINIMUM);
		}		
		
		System.out.println("bestValues[0] = " + bestValues[0]);
		System.out.println("bestValues[1] = " + bestValues[1]);
		System.out.println("bestValues[2] = " + bestValues[2]);
//		System.out.println("criteriaSequence.startsWith('T', 1): " + criteriaSequence.startsWith("T", 1));
//		System.out.println("criteriaSequence.startsWith('T', 0): " + criteriaSequence.startsWith("T", 0));
//		System.out.println("criteriaSequence.startsWith('C', 1): " + criteriaSequence.startsWith("C", 1));
//		System.out.println("criteriaSequence.startsWith('P', 2): " + criteriaSequence.startsWith("P", 2));
		
		System.out.println("PREFERENTIAL:");		
		Gene bestGene = generation.calculateGenerationAndReturnMultiCriteriaOptimalValue(endPointId, preferentialObjectiveFunction, bestValues, criteriaSequence);
		System.out.println("bestGene = " + bestGene);
		
		List<Point> returnedPoints = null;
		if (bestGene != null) {
			returnedPoints = bestGene.getPoints();
			int endIndex = 0;
			for (int i = 0; i < returnedPoints.size(); i++) {
				if (returnedPoints.get(i).getId() == endPointId) {
					endIndex = i;
					break;
				}
			}					
			return returnedPoints.subList(0, endIndex + 1);
		}
		return returnedPoints;		
		//return points;
	}
}
