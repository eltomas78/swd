package pl.edu.pwr.controller;

import java.util.Collections;
import java.util.Date;
import java.util.List;
import java.util.Locale;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.ResponseBody;

import jersey.repackaged.com.google.common.collect.Lists;
import pl.edu.pwr.model.Point;
import pl.edu.pwr.serwis.CriteriaService;

/**
 * Handles requests for the application home page.
 */
@Controller
public class HomeController {
	
	private static final Logger logger = LoggerFactory.getLogger(HomeController.class);
	
	@Autowired
	@Qualifier("preferedCriteriaService")
	private CriteriaService preferedCriteriaService;
	
	@Autowired
	@Qualifier("nonPreferedCriteriaService")
	private CriteriaService nonPreferedCriteriaService;	
	
	
	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String home(Locale locale, Model model) {
		logger.info("Welcome home! The client locale is {}.", locale);	
		
		return "home";
	}
	
	@RequestMapping(value = "/points", method = RequestMethod.PUT)
	public @ResponseBody List<Point> points(@RequestBody List<Point> points,
			@RequestParam String radioValue, @RequestParam String criteriaSequence,
			@RequestParam Integer startPointId, @RequestParam Integer endPointId) {
		logger.info("points.size: {}, radioValue: {}, criteriaSequence: {}, startPointId: {}, endPointId: {}, points: {}",
				points.size(), radioValue, criteriaSequence, startPointId, endPointId, points);
		
		if (radioValue.equals("nonpref")) {
			logger.info("NON_PREFERED_CRITERIA...");
			return nonPreferedCriteriaService.getOptimalRoad(startPointId, endPointId, criteriaSequence, points);
		} else {
			logger.info("PREFERED_CRITERIA...");
			return preferedCriteriaService.getOptimalRoad(startPointId, endPointId, criteriaSequence, points);
		}		
	}	
}
