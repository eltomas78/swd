package pl.edu.pwr;

import java.util.ArrayList;
import java.util.List;

import pl.edu.pwr.function.DistanceObjectiveFunction;
import pl.edu.pwr.function.EconomicObjectiveFunction;
import pl.edu.pwr.function.NonPreferentialObjectiveFunction;
import pl.edu.pwr.function.PreferentialObjectiveFunction;
import pl.edu.pwr.function.TimeObjectiveFunction;
import pl.edu.pwr.model.Gene;
import pl.edu.pwr.model.Generation;
import pl.edu.pwr.model.Point;
import pl.edu.pwr.model.Weights;

public class Runner {

	public static void main(String[] args) {
		int indexOfStartPoint = 2;
		int indexOfEndPoint = 4;
		int population = 10;
		int generationsQuantity = 10;
		int tournamentSize = 3;
		double mutationProbability = 0.01;
		double hybridizationProbability = 0.25;
		boolean minimum = true;
		int[] bestValues = new int[3];//distance = [0], time = [1], economic = [2]
		List<Point> points = new ArrayList<Point>();
		points.add(new Point(0, 2.33, 1.25));
		points.add(new Point(1, 2.20, 1.89));
		points.add(new Point(2, 2.98, 0.33));
		points.add(new Point(3, 3.33, 2.76));
		points.add(new Point(4, 1.22, 3.65));
		points.add(new Point(5, 2.45, 7.51));
		points.add(new Point(6, 3.52, 9.18));
		points.add(new Point(7, 4.55, 6.22));
		
		DistanceObjectiveFunction distanceObjectiveFunction = new DistanceObjectiveFunction();
		TimeObjectiveFunction timeObjectiveFunction = new TimeObjectiveFunction();
		EconomicObjectiveFunction economicObjectiveFunction = new EconomicObjectiveFunction();
		
		Generation generation = new Generation(population, mutationProbability, hybridizationProbability);
		generation.createRandomisedGeneration(indexOfStartPoint, points);
		
		
		/*while(generationsQuantity > 0){
			generationsQuantity--;
		}	*/	
		
		System.out.println("DISTANCE:");
		bestValues[0] = generation.calculateGenerationAndReturnExtremum(indexOfEndPoint, distanceObjectiveFunction, minimum);
		System.out.println("TIME:");
		bestValues[1] = generation.calculateGenerationAndReturnExtremum(indexOfEndPoint, timeObjectiveFunction, minimum);
		System.out.println("ECONOMIC:");
		bestValues[2] = generation.calculateGenerationAndReturnExtremum(indexOfEndPoint, economicObjectiveFunction, minimum);
		
		System.out.println("NON_PREFERENTIAL:");
		NonPreferentialObjectiveFunction nonPreferentialObjectiveFunction = new NonPreferentialObjectiveFunction();
		Gene bestGene1 = generation.calculateGenerationAndReturnMultiCriteriaOptimalValue(indexOfEndPoint, nonPreferentialObjectiveFunction, bestValues, "TCP");
		System.out.println("bestGene = " + bestGene1);
		
		System.out.println("PREFERENTIAL:");
		PreferentialObjectiveFunction preferentialObjectiveFunction = new PreferentialObjectiveFunction();
		Gene bestGene2 = generation.calculateGenerationAndReturnMultiCriteriaOptimalValue(indexOfEndPoint, preferentialObjectiveFunction, bestValues, "TCP");
		System.out.println("bestGene = " + bestGene2);
		
		/*System.out.println("MUTATION:");
		generation.mutate();
		System.out.println("CROSSOVER:");
		generation.crossover();
		System.out.println("NEW_GENERATION:");
		Generation newGeneration = generation.makeGeneSelection(tournamentSize, indexOfEndPoint, preferentialObjectiveFunction, bestValues);
		System.out.println("newGeneration: " + newGeneration);*/
		
		Weights weights = new Weights();
		Integer[][] matrix = weights.createTimeRandomMatrix(points);
		Integer[][] matrix2 = weights.createDistanceMatrix(points);
		System.out.println(weights.displayMatrix(matrix));
		System.out.println(weights.displayMatrix(matrix2));
	}
}
