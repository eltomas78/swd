package pl.edu.pwr.comparator;

import java.util.Comparator;

public class IntegerComparator implements Comparator<Integer> {

	@Override
	public int compare(Integer o1, Integer o2) {
		int result = o1 - o2;
		System.out.println("o1 = " + o1 + ", o2 = " + o2 + ", integer result = " + result);
		return result;
	}

}
