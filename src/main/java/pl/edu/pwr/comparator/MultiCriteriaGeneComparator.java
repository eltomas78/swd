package pl.edu.pwr.comparator;

import java.util.Comparator;

import pl.edu.pwr.function.MultiCriteriaObjectiveFunction;
import pl.edu.pwr.function.NonPreferentialObjectiveFunction;
import pl.edu.pwr.model.Gene;

public class MultiCriteriaGeneComparator implements Comparator<Gene> {
	private MultiCriteriaObjectiveFunction function;
	private int indexOfEndPoint;
	private int[] bestValues;
	private String criteriaSequence;
	
	public MultiCriteriaGeneComparator(int indexOfEndPoint, MultiCriteriaObjectiveFunction function, int[] bestValues, String criteriaSequence) {
		this.function = function;
		this.indexOfEndPoint = indexOfEndPoint;
		this.bestValues = bestValues;
		this.criteriaSequence = criteriaSequence;
	}

	@Override
	public int compare(Gene gene1, Gene gene2) {	
		if (function instanceof NonPreferentialObjectiveFunction) {
			Integer calculatedValueOfGene1 = gene1.calculateMultiCriteriaNonPrefFunction(indexOfEndPoint, function, bestValues, criteriaSequence);
			Integer calculatedValueOfGene2 = gene2.calculateMultiCriteriaNonPrefFunction(indexOfEndPoint, function, bestValues, criteriaSequence);
			int result = calculatedValueOfGene1 - calculatedValueOfGene2;
			//System.out.println("calculatedValueOfGene1 = " + calculatedValueOfGene1 + ", calculatedValueOfGene2 = " + calculatedValueOfGene2 + ", result = " + result);		
			return result;
		} else {
			int DUMMY_BEST_VALUES_INDEX = 0;
			Integer calculatedValueOfGene1 = gene1.calculateMultiCriteriaPrefFunction(indexOfEndPoint, function, bestValues, DUMMY_BEST_VALUES_INDEX, criteriaSequence);
			Integer calculatedValueOfGene2 = gene2.calculateMultiCriteriaPrefFunction(indexOfEndPoint, function, bestValues, DUMMY_BEST_VALUES_INDEX, criteriaSequence);
			int result = calculatedValueOfGene1 - calculatedValueOfGene2;
			//System.out.println("calculatedValueOfGene1 = " + calculatedValueOfGene1 + ", calculatedValueOfGene2 = " + calculatedValueOfGene2 + ", result = " + result);		
			return result;
		}
		
	}
}
