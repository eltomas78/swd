var map;
var markers = [];
var points = [];
var quantityMarkers = 10;
var startPointSelected = false;
var startPointId, endPointId;
var flightPath;
var locations = [
	['The British Library'       , 51.529145, -0.128317, 1],
	['Kings Place'               , 51.534164, -0.121794, 2],
	['ZSL London Zoo'            , 51.534804, -0.152006, 3],
	['The British Museum'        , 51.518678, -0.125570, 4],
	['Buckingham Palace'         , 51.503934, -0.140676, 5],
	['Big Ben'                   , 51.499874, -0.125227, 6],
	['Victoria and Albert Museum', 51.495706, -0.171576, 7],
	['Olympia'                   , 51.494851, -0.212774, 8],//start Point
	['Barbican Centre'           , 51.520066, -0.091753, 9],//end Point
	['City Hall'                 , 51.498912, -0.076818, 10]
 ];

function myMap() {
	var mapCanvas = document.getElementById("map");
	// [gora: 51.531000; dol: 51.482000][lewy: -0.237000; prawy: -0.005000]
	var southWest = new google.maps.LatLng(51.482000, -0.237000);
	var northEast = new google.maps.LatLng(51.531000, -0.005000);
	var lngSpan = northEast.lng() - southWest.lng();
	var latSpan = northEast.lat() - southWest.lat();

	var mapOptions = {
		center : new google.maps.LatLng(51.508742, -0.120850),
		zoom : 13
	};

	map = new google.maps.Map(mapCanvas, mapOptions);
	//randomiseMarkers(southWest, latSpan, lngSpan);
	setStaticMarkers();
}

var setStaticMarkers = function() {
	for (var i = 0; i < locations.length; i++) {
		var marker = new google.maps.Marker(
				{
					position : new google.maps.LatLng(locations[i][1], locations[i][2]),
					map : map,
					title : locations[i][0],
					id: i
				});

		// process multiple info windows
		(function(marker, i) {
			// add click event
			google.maps.event.addListener(marker, 'click', function() {
				infowindow = new google.maps.InfoWindow({
					content : 'id: ' + marker.id
				});
				infowindow.open(map, marker);
				setSelectedPoint(marker.id);
				checkObliczBtn();
			});
		})(marker, i);		

		var point = {
			id : i,
			xAxis : marker.position.lng(),
			yAxis : marker.position.lat()
		};
		points.push(point);
	}
}

var randomiseMarkers = function(southWest, latSpan, lngSpan) {
	for (var i = 0; i < quantityMarkers; i++) {
		var marker = new google.maps.Marker(
				{
					position : new google.maps.LatLng(southWest.lat() + latSpan
							* Math.random(), southWest.lng() + lngSpan
							* Math.random()),
					map : map,
					id: i
				});

		// process multiple info windows
		(function(marker, i) {
			// add click event
			google.maps.event.addListener(marker, 'click', function() {
				infowindow = new google.maps.InfoWindow({
					content : 'pos.lat: ' + marker.position.lat()
							+ ', pos.lng: ' + marker.position.lng()
							+ ', id: ' + marker.id
				});
				infowindow.open(map, marker);
				setSelectedPoint(marker.id);
				checkObliczBtn();
			});
		})(marker, i);		

		var point = {
			id : i,
			xAxis : marker.position.lng(),
			yAxis : marker.position.lat()
		};
		points.push(point);
	}
}

var drawLines = function(pointMarkers) {
	flightPath && flightPath.getPath().clear();
	flightPath = new google.maps.Polyline({
		path : pointMarkers,
		strokeColor : "#0000FF",
		strokeOpacity : 0.8,
		strokeWeight : 2
	});	
	flightPath.setMap(map);
}

var convertPointsToMarkers = function(points) {
	console.log("points: " + points.length);
	var pointMarkers = [];
	for (var i = 0; i < points.length; i++) {
		var latLng = new google.maps.LatLng(points[i].yAxis, points[i].xAxis);
		pointMarkers.push(latLng);
	}
	return pointMarkers;
}

var calculateRoad = function() {	
	var radioValue = getRadioValue();
	var selectValue = getSellectionValue();
	console.log('radio: ' + radioValue);
	console.log('select: ' + selectValue);
	console.log('startPointId: ' + startPointId);
	console.log('endPointId: ' + endPointId);
	$.ajax({
		contentType : "application/json",
		dataType : "json",
		headers : {
			"Content-Type" : "application/json",
			"Accept" : "application/json"
		},
		url : "/pwr/points?radioValue=" + radioValue + "&criteriaSequence=" + selectValue + "&startPointId=" + startPointId + "&endPointId=" + endPointId,
		data : JSON.stringify(points),
		type : "PUT",
		success : function(result, status) {
			console.log("Success! result: " + JSON.stringify(result)
					+ ", status: " + status);
			var pointMarkers = convertPointsToMarkers(result);
			drawLines(pointMarkers);
		},
		error : function(result, status) {
			console.log("Error! result: " + result + ", status: " + status);
		}
	});
}

var getRadioValue = function(){
	var radios = document.getElementsByName('inlineRadioOptions');	

	for (var i = 0, length = radios.length; i < length; i++) {
	    if (radios[i].checked) {	        
	        selectedRadio = radios[i].value
	        return radios[i].value;
	    }
	}
}

var radioClick = function(value){
	console.log('click value: ' + value);
	var btnDisabled = value === 'nonpref';
	document.getElementById('sellection').disabled = btnDisabled;	
}

var getSellectionValue = function() {
	var sellection = document.getElementById("sellection");
	return sellection.options[sellection.selectedIndex].value;
}

var setSelectedPoint = function(point) {
	if (!startPointSelected) {
		startPointId = point;
		startPointSelected = true;
		document.getElementById('startPointId').value = startPointId;
	} else {
		endPointId = point;
		document.getElementById('endPointId').value = endPointId;
	}	
}

var resetRoad = function() {
	points = [];	
	quantityMarkers = 10;
	startPointId = undefined;
	endPointId = undefined;
	startPointSelected = false;
	myMap();
	document.getElementById('quantityMarkers').value = quantityMarkers;
	document.getElementById('startPointId').value = startPointId;
	document.getElementById('endPointId').value = endPointId;
}

var checkObliczBtn = function() {
	if (startPointId == undefined || endPointId == undefined) {
		document.getElementById('obliczBtn').disabled = true;
	} else {
		document.getElementById('obliczBtn').disabled = false;
	}
}

var randomise = function() {
	quantityMarkers = document.getElementById('quantityMarkers').value;
	points = [];		
	startPointId = undefined;
	endPointId = undefined;
	startPointSelected = false;
	myMap();		
	document.getElementById('startPointId').value = startPointId;
	document.getElementById('endPointId').value = endPointId;
}