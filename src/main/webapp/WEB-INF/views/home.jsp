<%@ taglib uri="http://java.sun.com/jsp/jstl/core" prefix="c"%>
<%@ page session="false"%>
<%@ taglib prefix="spring" uri="http://www.springframework.org/tags"%>
<!DOCTYPE html>
<html lang="pl">
<head>
<meta charset="utf-8">
<meta http-equiv="X-UA-Compatible" content="IE=edge">
<meta name="viewport" content="width=device-width, initial-scale=1">
<!--  <meta name="viewport" content="initial-scale=1, maximum-scale=1, user-scalable=no" /> -->
<meta http-equiv="Content-Type" content="text/html; charset=UTF-8">

<spring:url value="/resources/lib/jquery/jquery-3.1.1.min.js"
	var="jqueryJs" />
<spring:url value="/resources/lib/angular/angular.min.js"
	var="angularJs" />
<spring:url value="/resources/lib/angular/angular-route.min.js"
	var="angularRouteJs" />
<spring:url value="/resources/lib/bootstrap/js/bootstrap.min.js"
	var="bootstrapJs" />
<spring:url value="/resources/app/utils.js" var="utilsJs" />

<spring:url value="/resources/lib/bootstrap/css/bootstrap.min.css"
	var="bootstrapCss" />
<spring:url value="/resources/lib/bootstrap/css/bootstrap-theme.min.css"
	var="bootstrapThemeCss" />

<!-- libraries -->
<script src="${jqueryJs}"></script>
<script src="${angularJs}"></script>
<script src="${angularRouteJs}"></script>
<script src="${bootstrapJs}"></script>

<!-- utils -->
<script src="${utilsJs}"></script>

<link rel="stylesheet" href="${bootstrapCss}">
<link rel="stylesheet" href="${bootstrapThemeCss}">

<title>SWD - Wielokryterialne podejmowanie decyzji</title>

</head>
<body>
	<div class="container-fluid">
		<div class="row">
			<h2>Wybierz kryteria:</h2>

			<div class="form-group">
				<label class="radio-inline">
					<input type="radio" name="inlineRadioOptions" id="radioNonPref" value="nonpref"
						checked="checked" onclick="radioClick(this.value)">Metoda niepreferencyjna
				</label>
				<label class="radio-inline">
					<input type="radio"	name="inlineRadioOptions" id="radioPref" value="pref"
						onclick="radioClick(this.value)">Metoda preferencyjna
				</label>
				<label for="quantityMarkers">Ilosc punktow:
					<input type="number" class="form-control" id="quantityMarkers" value="10"
						min="10" max="100">
				</label>
				<label for="startPointId">Punkt startowy:
					<input type="number" class="form-control" id="startPointId" readonly="readonly">
				</label>
				<label for="endPointId">Punkt koncowy:
					<input type="number" class="form-control" id="endPointId" readonly="readonly">
				</label>
				<select class="form-control"
					id="sellection" disabled>
					<option value="TCP">Trasa/Czas/Paliwo</option>
					<option value="TPC">Trasa/Paliwo/Czas</option>
					<option value="PCT">Paliwo/Czas/Trasa</option>
					<option value="PTC">Paliwo/Trasa/Czas</option>
					<option value="CTP">Czas/Trasa/Paliwo</option>
					<option value="CPT">Czas/Paliwo/Trasa</option>
				</select>

			</div>


		</div>

		<div class="row">
			<div id="map" style="width: 100%; height: 500px"></div>
		</div>
		<button type="button" class="btn btn-danger" onclick="resetRoad()">Resetuj</button>
		<button type="button" class="btn btn-info" onclick="randomise()">Losuj</button>
		<button type="button" class="btn btn-success" id="obliczBtn" disabled
			onclick="calculateRoad()">Oblicz</button>
	</div>


	<script
		src="https://maps.googleapis.com/maps/api/js?key=AIzaSyAqiWPgqMbN6ynDTDmdrXjU2LGlP3DxRW0&callback=myMap"></script>
</body>
</html>
